require 'date'
require 'bitbucket_rest_api'
require 'methadone'

module Git2bit
	class BitbucketProxy
	  include Methadone::Main
	  include Methadone::CLILogging

	  attr_reader :components, :milestones

		# Possible values from BitBucket
		STATUS = ['new','open','resolved','on hold','invalid','duplicate','wontfix']
		KINDS = ['bug','enhancement','proposal','task']
		PRIORITIES = ['trivial','minor','major','critical','blocker']

		def initialize(args)
			@repo = args[:repo]
			@user = args[:user]
			@org = args[:org]
		    if @org
		      @owner = @org
		    else
		      @owner = @user
		    end
			

			# Connect to BitBucket
			begin
				@conn = BitBucket.new :basic_auth => "#{@user}:#{args[:pass]}", :user => @user, :repo => @repo

				# Store Milestones to search upon later on
				@milestones = Array.new
				@conn.issues.milestones.list(@owner, @repo).each {|m| @milestones.push m.name}
				debug "BitBucket Milestones:\n - " + @milestones.join("\n - ")

				# Store Components to search upon later on
				@components = Array.new
				@conn.issues.components.list(@owner, @repo).each {|c| @components.push c.name}
				debug "BitBucket Components:\n - " + @components.join("\n - ")

				info "Successfully connected to BitBucket #{@user}/#{@repo}"
	    rescue Exception => e 
	      error = ["Error: Unable to connect to BitBucket #{@user}/#{@repo}", e.message].push e.backtrace
	      exit_now!(error.join("\n"))
	    end
		end

		def create_milestone(title)
			begin
				# Create the milestone in BitBucket
        milestone = @conn.issues.milestones.create @owner, @repo, { :name => title }
       
        # Add it to known milestones
				@milestones.push title

				info "BitBucket: created milestone '#{title}'"
	    rescue Exception => e 
	      error "Error: Unable to create to BitBucket milestone '#{title}' - " + e.message
	      debug e.backtrace.join("\n")
	    end
	    milestone
		end

		def create_issue(args)
			begin
	      # Create the Bitbucket issue
	      issue = @conn.issues.create @owner, @repo, {
	        :title => args[:title], 
	        :content => args[:content], 
	        :responsible => args[:responsible], 
	        :milestone => args[:milestone], 
	        :component => args[:component],
	        :priority => args[:priority],
	        :status => args[:status],
	        :kind => args[:kind]
	      }
				info "BitBucket: created issue ##{issue.local_id} '#{args[:title]}'"
	    rescue Exception => e 
	      error "Error: Unable to create to BitBucket issue '#{args[:title]}' - " + e.message
	      debug e.backtrace.join("\n")
	    end
	    issue.local_id
		end

		def create_comment(id,content)
			begin
	      # Create the Bitbucket comment
      	comment = @conn.issues.comments.create @owner, @repo, id, {:content => content}
				info "BitBucket: created comment for issue ##{id}"
	    rescue Exception => e 
	      error "Error: Unable to create to BitBucket comment for issue ##{id} - " + e.message
	      debug e.backtrace.join("\n")
	    end
	    comment
		end

		def analyze_labels(labels)
			debug "Analyzing labels: " + labels.inspect

			# The tag values that we are going to try to discover
			tags = { :component => nil, :priority => nil, :kind => nil, :status => nil, :labels => Array.new }
			
			labels.each do |label|
				# Store the label name
				tags[:labels].push label

				name = label.downcase
				
				# Look for a priority in available fields in BB_PRIORITIES
				if tags[:priority].nil? and PRIORITIES.find_index(name)
					tags[:priority] = name
					next
				end

				# Look for a kind in available fields in BB_KINDS
				if tags[:kind].nil? and KINDS.find_index(name)
					tags[:kind] = name
					next
				end

				# Look for a status in available fields in BB_STATUS
				if tags[:status].nil? and STATUS.find_index(name)
					tags[:status] = name
					next
				end

				# Look for a component. Need to do case insensitive match on each known component.
				if tags[:component].nil?
					components.each do |c|
						if c.match(/#{name}/i)
							tags[:component] = c
							break
						end
					end
				end

			end 
			debug "Built Tags: " + tags.inspect

			tags
		end


	end
end
