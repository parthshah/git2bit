# git2bit

There are many people out there that have migrated their private repositories from Github to Bitbucket. Bitbucket has a goo utility that will clone your entire repository over, however, there is no way to migrate any existing issues that you have on Github. 

git2bit is a CLI that solves this by migrating all your issues along with their comments and milestones from Github into your new Bitbucket repo. 

**Important Tip**: I recommend that you create a new blank repo in Bitbucket to run intial migration tests with. Once you are happy with the migration results, you can delete this repository and proceed with migrating issues to the production repo.

## How does it work?

The issues systems between Github and Bitbucket are similar but also pretty different. Github uses *labels* in order to get you to organize and categorize your issues. Bitbucket does not support labels and instead has a more traditional approach using  a predefined set of fields. 

Obviously the issue title and body will come over unchanged. However, all of the migrated issues will be created by the Bitbucket user account that is configred in git2bit. The original creation date of the issue will not be migrated either. As a consolation, the original data from Github will be inserted at the top of the newly created issue.  

	Github issue #459 created by joeworkman on 2012-09-06 20:51:49

Here are some more caveats with each Bitbucket issue field. 

**Tip**: Chances are you already had labels in Github for organizing issues by kind, priority, or component. Before you migrate issues using git2bit, make sure that you rename your labels to maps to the supported Bitbucket fields documented below. 

### Comments

The first comment added to newly created issues will contain a list of all the Github labels that were assigned to the original ticket. This will ensure that you still have this data if its valuable to you. 

All comments from Github will be brought over and inserted into the new issue created on Bitbucket. Just like at the ticket level the creation data (username & date) will be inserted at the top of each comment. 

### Kind

Bitbucket supports 4 different kinds of issues: `bug`, `enhancement`, `proposal`, and `task`. 

git2bit will evaluate all of the labels that are assigned to each Github issue. If one of the labels matches the mentioned Bitbucket kinds, when the issue if created, it will be assigned to that kind. If no matching label is found, the issue will default to be a bug. 

### Priority

Bitbucket supports 5 different priorities for issues: `trivial`, `minor`, `major`, `critical`, and `blocker`. 

git2bit will evaluate all of the labels that are assigned to each Github issue. If one of the labels matches the mentioned Bitbucket priorities, when the issue if created, it will be assigned to that priority. If no matching label is found, the issue will default to the priority of trivial. 

### Status

Bitbucket supports 7 different priorities for issues: `new`, `open`, `resolved`, `on hold`, `invalid`, `duplicate`, and `wontfix`.

This somewhat overlaps with Github issue status. In Github an issue can only ever be opened or closed. However, I know for a fact that developers use labels to get better fine-grained control over this. Therefore, git2bit evaluates the labels on each Github issue. If one of the labels matches the mentioned Bitbucket status options, when the issue if created, it will be assigned to that status. If no matching label is found, then the issue will be set to open. 

**Note**: The `closed` status in a Github issue will trump everything. In this case, the newly created issue will be set to `resolved` in Bitbucket.

### Component

Bitbucket has support for manually defined components. If you would like for git2bit to populate this field for an issue, you will need to make sure that all of the Components are defined in your repository's Issues settings. 

git2bit will evaluate all of the labels that are assigned to each Github issue. If one of the labels matches a preconfigured Component, the newly created issue will be assigned to that Component. If no matching label is found, no Components will be assigned. 

**Note**: Bitbucket only allows an issue to be associated with one Component. Therefore, if you were using Github labels to assign multiple Components to an issue, when git2bit processes an issue, the first Component found within the labels wins. 

### Milestone

git2bit will honor the assignment of an issue to a milestone. If a mielstone does not exist in Bitbucket, it will be created. This ensure that when the new issue is created, its properly associated with the same milestones as it was in Github.

## Installation

Install it via the gem command in terminal:

    $ gem install git2bit

You can now execute it:

    $ git2bit --help

## Usage

Here is an overview of the options available to git2bit along with some helpful examples. See below for how to set these values using a `.git2bit.rc` config file. 

    Usage: git2bit [options]

    Options:
        -h, --help                 Show command line help
            --version              Show help/version info
            --[no-]closed          [Do not] migrate closed issues
            --issue ISSUE_NUMBER   Process only a single Github issue number
            --guser USER           Github username
            --gpass PASSWORD       Github password
            --grepo REPO           Github repo name
            --gorg ORGANIZATION    Github organization name
            --buser USER           Bitbucket username
            --bpass PASSWORD       Bitbucket password
            --brepo REPO           Bitbucket repo name
            --borg ORGANIZATION    Bitbucket organization name
            --log-level LEVEL      Set the logging level
                                   (debug|info|warn|error|fatal)
                                   (Default: info)

The following examples assume that you have the following options configured in your `.git2bit.rc` file: `guser`, `gpass`, `grepo`, `buser`, `bpass`, `brepo`

Move *all* issues (open and closed):

	$ git2bit

Move only *open* issues: 

	$ git2bit --no-close

Move a single issue:

	$ git2bit -i 532

### .git2bit.rc

Instead of passing a ton of options via command line, you can setup the configuration values in a file named `.git2bit.rc`. This file should live in your home directory.

Here is a sample file to get you going. 

    guser: joeworkman
    gpass: p@ssw0rd
    grepo: myrepo

    buser: joeworkman
    bpass: p@ssw0rd
    brepo: newrepo


## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request

## Thank You

git2bit uses the following awesome Ruby Gems:

* github_api - <https://github.com/peter-murach/github>
* bitbucket_rest_api - <https://github.com/exceptionary/bitbucket>